# A-Mazing Urho asset licenses

## Used licenses
_[CC0](https://creativecommons.org/publicdomain/zero/1.0/), [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/), [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)_

### Various
##### Author irrelevant
- CC0
    + Docs/*
    + Screenshots/*
    + Resources/Materials/*
    + Resources/Shaders/*
    + Resources/Techniques/*
    + Resources/RenderPaths/*
    + Resources/PostProcess/*
    + Resources/Textures/Ramp.*
    + Resources/Textures/Spot.*
    + Resources/Maps/Blockset.ebs
    
### Music
##### Konstantin 'kostik1337' Guschin
- CC-BY 4.0
    + Resources/Music/*

### Samples
##### Konstantin 'kostik1337' Guschin
- CC-BY 4.0
    + Resources/Samples/Crack.ogg
    + Resources/Samples/Hit.ogg

##### Modanung
- CC-BY 4.0
    + Resources/Samples/Bump.ogg
    + Resources/Samples/Swim*.ogg
    + Resources/Samples/Warp.ogg
    + Resources/Samples/Select.ogg
    + SCCode/Samples.scd
    
### 2D
##### Jovanny Lemonad & Erico Lebedenco
- CC0
    + Resources/Fonts/Philosopher.ttf [[Source]](https://www.dafont.com/philosopher.font)
    
##### Emil 'Humus' Persson (blurred by 1vanK)
- CC-BY 3.0
    + Resources/Textures/EnvBlurred*

##### Abby_Holzworth1 
- CC-BY 4.0
    + Resources/Textures/Anglerfish*
    
##### Modanung
- CC-BY-SA 4.0
    + SVGs/Buttons.svg
    + SVGs/Title.svg
    + Resources/Textures/Buttons.png
    + Resources/Textures/Title.png
    + Resources/Textures/Water.png
    + Resources/Textures/Urho*   
    + amazingurho.png
    + Resources/icon.png
        

## 3D
##### Abby_Holzworth1 
- CC-BY 4.0
    + Resources/Models/Anglerfish.mdl
    + Blends/Anglerfish.blend [[Source]](https://sketchfab.com/3d-models/anglerfish-5e2f4b2d2e1a4613a008b6caaf40d50c)
    
##### Modanung
- CC-BY-SA 4.0
    + Resources/Models/:.mdl
    + Resources/Models/0.mdl
    + Resources/Models/1.mdl
    + Resources/Models/2.mdl
    + Resources/Models/3.mdl
    + Resources/Models/4.mdl
    + Resources/Models/5.mdl
    + Resources/Models/6.mdl
    + Resources/Models/7.mdl
    + Resources/Models/8.mdl
    + Resources/Models/9.mdl
    + Resources/Models/AmazingUrho.mdl
    + Resources/Models/Barrel.mdl
    + Resources/Models/Cornerstone.mdl
    + Resources/Models/Crab.mdl
    + Resources/Models/CrabIdle.ani
    + Resources/Models/CrabWalk.ani
    + Resources/Models/Jelly.mdl
    + Resources/Models/JellySwim.ani
    + Resources/Models/PathBend.mdl
    + Resources/Models/PathCrossing.mdl
    + Resources/Models/PathFork.mdl
    + Resources/Models/PathStraight.mdl
    + Resources/Models/Pit.mdl
    + Resources/Models/Plank_Compete.mdl
    + Resources/Models/Plank_Exit.mdl
    + Resources/Models/Plank_Play.mdl
    + Resources/Models/Pole.mdl
    + Resources/Models/Portal.mdl
    + Resources/Models/Rock.mdl
    + Resources/Models/Shrimp.mdl
    + Resources/Models/SpeedoCase.mdl
    + Resources/Models/SpeedoHand.mdl
    + Resources/Models/Swim.ani
    + Resources/Models/Terrain.mdl
    + Resources/Models/Urho.mdl
    + Resources/Models/Urho.txt
    + Resources/Models/Weed.mdl
    + Resources/Models/Urchin.mdl
    + Blends/HUD.blend
    + Blends/Jelly.blend
    + Blends/Menu.blend
    + Blends/World.blend
    
## Maps
##### Modanung
- CC-BY 4.0
    + Resources/Maps/1\_Initiation\_61.emp
    + Resources/Maps/3\_Roger\_120.emp
    + Resources/Maps/4\_Totter\_60.emp
    + Resources/Maps/5\_Naught\_42.emp
    + Resources/Maps/6\_Greyhound\_235.emp
    + Resources/Maps/7\_SpiralTap\_90.emp
    + Resources/Maps/Colourship\_0.emp
    
- CC0
    + Resources/Maps/Blockset.ebs