/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include "luckey.h"

namespace Dry {
class Node;
class Scene;
}

class Jib;
class MainMenu;
class Maze;
class Player;

struct CameraProperties
{
    Vector3 position_;
    Vector3 lookTarget_;
    float fieldOfView_;
};

enum GameState{ GS_MENU, GS_PLAY, GS_WIN, GS_LOSE };

class MasterControl: public Application
{
    DRY_OBJECT(MasterControl, Application);

public:
    MasterControl(Context* context);

    Scene* GetScene() const { return scene_; }
    Maze* GetMaze() const { return maze_; }
    Jib* GetJib() const { return jib_; }

    void AddPlayer();
    Player* GetPlayer(int playerId) const;
    Player* GetNearestPlayer(const Vector3& pos);
    Vector< SharedPtr<Player> > GetPlayers();
    void RemovePlayer(Player* player);

    void Setup() override;
    void Start() override;
    void Stop() override;
    void Exit();

    template <class T> void RegisterObject() { context_->RegisterFactory<T>(); }
    template <class T> void RegisterSubsystem() { context_->RegisterSubsystem(new T(context_)); }
    float Sine(const float freq = 1.f, const float min = -1.f, const float max = 1.f, const float shift = 0.f);
    float Cosine(const float freq = 1.f, const float min = -1.f, const float max = 1.f, const float shift = 0.f);

    void SetCompete(bool enabled) { compete_ = enabled; }
    bool IsMultiplayer() const { return compete_; }

    void SetGameState(GameState state);
    GameState GetGameState() const { return gameState_; }
    void StartLevel(const String& mazeFileName);
    void StartNewGame();
    void StartNextLevel();
    void RestartLevel();
    void Winner();
    void TimeUp();

    void HandleScreenMode(StringHash eventType, VariantMap& eventData);


private:
    void SetupDefaultRenderpath();
    void CreateScene();
    void CreateLights();

    GameState gameState_;
    Scene* scene_;
    Jib* jib_;
    MainMenu* mainMenu_;
    Maze* maze_;
    Vector< SharedPtr<Player> > players_;
    Vector< Node* > spotNodes_;
    Sound* ingameIntro_;
    Sound* ingameLoop_;
    StringVector levels_;
    int level_;
    bool compete_;

    float SinePhase(float freq, float shift);
    void HandleSoundFinished(StringHash eventType, VariantMap& eventData);
    void HandleArguments();
    void FindLevels();

    Text* tempMessage_;
};

#endif // MASTERCONTROL_H
