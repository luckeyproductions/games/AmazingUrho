/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SPAWNMASTER_H
#define SPAWNMASTER_H

#include "mastercontrol.h"

class SceneObject;

class SpawnMaster: public Object
{
    friend class MasterControl;
    DRY_OBJECT(SpawnMaster, Object);

public:
    SpawnMaster(Context* context);

    void Clear();

    template <class T> T* Create(bool recycle = true)
    {
        if (recycle)
            for (Node* n: GetAllNodes<T>())
                if (!n->IsEnabled())
                    return n->GetComponent<T>();

        Node* spawnedNode{ MC->GetScene()->CreateChild(T::GetTypeNameStatic()) };
        T* created{ spawnedNode->CreateComponent<T>() };
        spawnedNode->SetEnabledRecursive(false);

        if (!created_.Contains(T::GetTypeStatic()))
            created_[T::GetTypeStatic()] = PODVector<SceneObject*>{};

        created_[T::GetTypeStatic()].Push(created);

        return created;
    }

    template <class T> PODVector<T*> GetAll() const
    {
        PODVector<SceneObject*> all{  };
        if (!created_.TryGetValue(T::GetTypeStatic(), all))
            return {};

        PODVector<T*> typed{};

        for (unsigned i{ 0 }; i < all.Size(); ++i)
            typed.Push(static_cast<T*>(all.At(i)));

        return typed;
    }

    template <class T> PODVector<T*> GetActive() const
    {
        PODVector<T*> components{ GetAll<T>() };

        for (T* c: components)
            if (!c->GetNode()->IsEnabled())
                components.RemoveSwap(c);

        return components;
    }

    template <class T> PODVector<Node*> GetAllNodes() const
    {
        PODVector<Node*> nodes{};
        PODVector<T*> all{ GetAll<T>() };

        for (unsigned i{ 0 }; i < all.Size(); ++i)
            nodes.Push(all.At(i)->GetNode());

        return nodes;
    }

    template <class T> PODVector<Node*> GetActiveNodes() const
    {
        PODVector<Node*> nodes{ GetAllNodes<T>() };

        for (Node* n: nodes)
            if (!n->IsEnabled())
                nodes.RemoveSwap(n);

        return nodes;
    }

    template <class T> int CountActive() const
    {
        return GetActiveNodes<T>().Size();
    }

private:
    HashMap<StringHash, PODVector<SceneObject*> > created_;
};

#endif // SPAWNMASTER_H
