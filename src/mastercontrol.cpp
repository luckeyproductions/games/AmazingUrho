/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "effectmaster.h"
#include "inputmaster.h"
#include "player.h"
#include "spawnmaster.h"
#include "jib.h"
#include "urho.h"
#include "shrimp.h"
#include "ui/mainmenu.h"
#include "ui/gui3d.h"
#include "environment/block.h"
#include "environment/weed.h"
#include "environment/portal.h"
#include "environment/maze.h"
#include "environment/waterspotlight.h"
#include "environment/colourship.h"
#include "hazards/barrelfactory.h"
#include "hazards/barrel.h"
#include "hazards/jelly.h"
#include "hazards/crab.h"
#include "hazards/anglerfish.h"
#include "hazards/urchin.h"

#include "mastercontrol.h"

DRY_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl::MasterControl(Context* context): Application(context),
    gameState_{ GS_MENU },
    scene_{ nullptr },
    jib_{ nullptr },
    mainMenu_{ nullptr },
    maze_{ nullptr },
    players_{},
    spotNodes_{},
    ingameIntro_{ nullptr },
    ingameLoop_{ nullptr },
    levels_{},
    level_{ -1 },
    compete_{ false },
    tempMessage_{ nullptr }
{
}

void MasterControl::Setup()
{
    SetRandomSeed(TIME->GetSystemTime());

    FileSystem* fs{ GetSubsystem<FileSystem>() };

    engineParameters_[EP_LOG_NAME] = fs->GetAppPreferencesDir("luckey", "amazingurho") + "AmazingUrho.log";
    engineParameters_[EP_WINDOW_TITLE] = "A-Mazing Urho";
    engineParameters_[EP_WINDOW_ICON] = "icon.png";
    engineParameters_[EP_WORKER_THREADS] = true;

    //Add resource paths
    String resourcePath{ "Resources" };
    if (!fs->DirExists(AddTrailingSlash(fs->GetProgramDir()) + resourcePath))
    {
        const String installedResources{ RemoveTrailingSlash(RESOURCEPATH) };
        if (fs->DirExists(installedResources))
            resourcePath = installedResources;
    }

    engineParameters_[EP_RESOURCE_PATHS] = resourcePath;

//    engineParameters_[EP_FULL_SCREEN] = false;
    engineParameters_[EP_WINDOW_RESIZABLE] = true;
}

void MasterControl::Start()
{
    context_->RegisterSubsystem(this);
    RegisterSubsystem<EffectMaster>();
    RegisterSubsystem<InputMaster>();
    RegisterSubsystem<SpawnMaster>();

    RegisterObject<Jib>();
    GUI3D::RegisterObject(context_);
    RegisterObject<Block>();
    RegisterObject<Portal>();
    RegisterObject<Weed>();
    RegisterObject<Maze>();
    RegisterObject<Urho>();
    RegisterObject<Shrimp>();
    RegisterObject<BarrelFactory>();
    RegisterObject<Barrel>();
    RegisterObject<Jelly>();
    RegisterObject<Crab>();
    RegisterObject<Anglerfish>();
    RegisterObject<Urchin>();

    RegisterObject<WaterSpotLight>();
    RegisterObject<Colourship>();
    RegisterObject<MainMenu>();

    if (GRAPHICS)
        ENGINE->SetMaxFps(GRAPHICS->GetRefreshRate());

    ingameIntro_ = CACHE->GetResource<Sound>("Music/IngameIntro.ogg");
    ingameIntro_->SetLooped(false);
    ingameLoop_ = CACHE->GetResource<Sound>("Music/IngameLoop.ogg");
    ingameLoop_->SetLooped(true);

    SetupDefaultRenderpath();
    FindLevels();
    CreateScene();

    tempMessage_ = GetSubsystem<UI>()->GetRoot()->CreateChild<Text>();
    tempMessage_->SetFont(RES(Font, "Fonts/Philosopher.ttf"));
    tempMessage_->SetTextAlignment(HA_CENTER);
    tempMessage_->SetFontSize(42.f);
    tempMessage_->SetColor(Color::CYAN);
    tempMessage_->SetTextEffect(TE_SHADOW);
    tempMessage_->SetEffectColor(Color::BLACK.Lerp(Color::TRANSPARENT_BLACK, .7f));
    tempMessage_->SetEffectShadowOffset({ 2, 3 });
    tempMessage_->SetAlignment(HA_CENTER, VA_CENTER);
    tempMessage_->SetVisible(false);

    HandleArguments();
    SubscribeToEvent(E_SCREENMODE, DRY_HANDLER(MasterControl, HandleScreenMode));
}

void MasterControl::Stop()
{
    engine_->DumpResources(true);
}

void MasterControl::Exit()
{
    engine_->Exit();
}

void MasterControl::SetupDefaultRenderpath()
{
    RenderPath* effectRenderPath{RENDERER->GetDefaultRenderPath()};
    effectRenderPath->Append(CACHE->GetResource<XMLFile>("PostProcess/FXAA3.xml"));
    effectRenderPath->SetEnabled("FXAA3", true);

    effectRenderPath->Append(CACHE->GetResource<XMLFile>("PostProcess/BloomHDR.xml"));
    effectRenderPath->SetShaderParameter("BloomHDRThreshold", .34f);
    effectRenderPath->SetShaderParameter("BloomHDRMix", Vector2{ .95f, .23f });
    effectRenderPath->SetEnabled("BloomHDR", true);
}

void MasterControl::FindLevels()
{
    const String mapsDir{ AddTrailingSlash(CACHE->GetResourceDirs().Front() + "Maps") };
    Log::Write(LOG_INFO, mapsDir);
    StringVector res{};
    FILES->ScanDir(res, mapsDir, "*.emp", SCAN_FILES, true);
    StringVector timed{};
    for (const String& m: res)
    {
        if (m.Contains("_"))
            timed.Push(mapsDir + m);
    }

    Sort(timed.Begin(), timed.End());
    levels_ = timed;
}

void MasterControl::CreateScene()
{
    scene_ = new Scene{ context_ };
    scene_->CreateComponent<Octree>();
    scene_->CreateComponent<DebugRenderer>();
    scene_->CreateComponent<PhysicsWorld>()->SetUpdateEnabled(false);

    GUI3D* gui3d{ scene_->CreateChild("3DGUI")->CreateComponent<GUI3D>() };
    context_->RegisterSubsystem(gui3d);
    gui3d->SetEnabled(false);

    Zone* zone{ scene_->CreateComponent<Zone>() };
    zone->SetBoundingBox({ { -100.f, -50.f, -100.f },
                           {  100.f,  50.0f, 100.f } });
    zone->SetAmbientColor({ .1f, .17f, .23f });
    zone->SetFogStart(23.f);
    zone->SetFogEnd(235.f);
    zone->SetFogColor(zone->GetAmbientColor());

    //Lights
    CreateLights();

    //The menu
    mainMenu_ = scene_->CreateChild("Menu")->CreateComponent<MainMenu>();
    //Camera
    jib_ = scene_->CreateChild("Camera")->CreateComponent<Jib>();
    jib_->Set(mainMenu_->GetCameraProperties());

    //A Maze!
    maze_ = scene_->CreateChild("Maze")->CreateComponent<Maze>();

    //Music!
    Sound* music{ CACHE->GetResource<Sound>("Music/Menu.ogg") };
    music->SetLooped(true);
//    music->SetLoop();
    SoundSource* musicSource{ scene_->CreateComponent<SoundSource>() };
//    musicSource->SetGain(0.0f); /////
    musicSource->SetSoundType(SOUND_MUSIC);
    musicSource->Play(music);
}

void MasterControl::CreateLights()
{
    Node* pointLightNode{ scene_->CreateChild("Light") };
    pointLightNode->SetPosition({ 3.f, 13.f, 2.f });
    pointLightNode->LookAt(Vector3::DOWN);
    Light* pointLight{ pointLightNode->CreateComponent<Light>() };
    pointLight->SetRange(55.0f);
    pointLight->SetBrightness(0.42f);

    Node* sunLightNode{ scene_->CreateChild("Light") };
    sunLightNode->SetPosition({ -3.f, 10.f, -1.f });
    sunLightNode->LookAt(Vector3::ZERO);
    Light* sunLight{ sunLightNode->CreateComponent<Light>() };
    sunLight->SetLightType(LIGHT_DIRECTIONAL);
    sunLight->SetCastShadows(true);
    sunLight->SetShadowIntensity(.666f);
    sunLight->SetShadowDistance(17.f);
    sunLight->SetRange(23.f);
    sunLight->SetBrightness(.42f);
    sunLight->SetSpecularIntensity(0.f);
    sunLight->SetColor({ .6f, .9f, 1.f });

    for (bool turned: { false, true })
    {
        Node* spotLightNode{ scene_->CreateChild("Light") };
        spotLightNode->CreateComponent<WaterSpotLight>()->Set(turned);
        spotNodes_.Push(spotLightNode);
    }
}

void MasterControl::HandleArguments()
{
    for (const String& arg: GetArguments())
    {
        if (arg.StartsWith("-"))
            continue;

        String extraDir{ arg };
        String mapName{ arg };

        if (arg.EndsWith(".emp", false))
        {
            const unsigned mPos{ arg.Find("Maps") };
            if (mPos > 0 && mPos != String::NPOS)
            {
                extraDir = arg.Substring(0, mPos - 1);
                mapName  = arg.Substring(extraDir.Length());
            }
        }
        else
        {
            mapName = "";
        }

        ResourceCache* cache{ GetSubsystem<ResourceCache>() };
        cache->AddResourceDir(extraDir, 0);

        if (!mapName.IsEmpty() && RES(XMLFile, mapName))
        {
            StartLevel(mapName);
            return;
        }
    }
}

void MasterControl::HandleScreenMode(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    if (!mainMenu_->IsEnabled())
        jib_->Set(maze_->GetCameraProperties(), false);
}

void MasterControl::StartNewGame()
{
    level_ = -1;
    StartNextLevel();
}

void MasterControl::StartNextLevel()
{
    if (level_ < static_cast<int>(levels_.Size()) - 1)
        StartLevel(levels_.At(++level_));
}

void MasterControl::StartLevel(const String& mazeFileName)
{
    mainMenu_->SetEnabled(false);
    Zone* zone{ scene_->GetComponent<Zone>() };
    zone->SetFogStart(M_INFINITY);
    zone->SetFogEnd(M_INFINITY);
    maze_->LoadBlockMap(mazeFileName);
    jib_->Set(maze_->GetCameraProperties());

    const unsigned lastUnderscore{ mazeFileName.FindLast("_") };
    const unsigned seconds{ ToUInt(mazeFileName.Substring(lastUnderscore + 1, mazeFileName.FindLast(".") - lastUnderscore - 1)) };

    GetSubsystem<GUI3D>()->StartLevel(seconds);

    scene_->GetComponent<SoundSource>()->Play(ingameIntro_);
    SubscribeToEvent(scene_, E_SOUNDFINISHED, DRY_HANDLER(MasterControl, HandleSoundFinished));

    SetGameState(GS_PLAY);
}

void MasterControl::RestartLevel()
{
    maze_->LoadBlockMap(levels_.At(level_));
    jib_->DisableBlur();
    GetSubsystem<GUI3D>()->RestartLevel();

    scene_->GetComponent<SoundSource>()->Play(ingameIntro_);
    SubscribeToEvent(scene_, E_SOUNDFINISHED, DRY_HANDLER(MasterControl, HandleSoundFinished));

    SetGameState(GS_PLAY);
}

void MasterControl::HandleSoundFinished(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    SoundSource* musicSource{ scene_->GetComponent<SoundSource>() };

    if (musicSource->GetSound()->GetName().Contains("Ingame"))
    {
        musicSource->Play(ingameLoop_);
        UnsubscribeFromEvent(scene_, E_SOUNDFINISHED);
    }
}

void MasterControl::Winner()
{
    SoundSource* musicSource{ scene_->GetComponent<SoundSource>() };

    Sound* music{ CACHE->GetResource<Sound>("Music/VictoriousFanfare.ogg") };
    music->SetLooped(false);
    musicSource->Play(music);

    SetGameState(GS_WIN);
}

void MasterControl::TimeUp()
{
    SoundSource* musicSource{ scene_->GetComponent<SoundSource>() };
    musicSource->Stop();

    SetGameState(GS_LOSE);
}

Vector<SharedPtr<Player> > MasterControl::GetPlayers()
{
    return players_;
}

Player* MasterControl::GetPlayer(int playerId) const
{
    for (Player* p: players_)
    {
        if (p->GetPlayerId() == playerId)
            return p;
    }

    return nullptr;
}

Player* MasterControl::GetNearestPlayer(const Vector3& pos)
{
    Player* nearest{};
    for (Player* p : players_)
    {
        if (p->IsAlive())
        {
            if (!nearest ||
                (LucKey::Distance(GetSubsystem<InputMaster>()->GetControllableByPlayer(p->GetPlayerId())->GetWorldPosition(), pos) <
                 LucKey::Distance(GetSubsystem<InputMaster>()->GetControllableByPlayer(nearest->GetPlayerId())->GetWorldPosition(), pos)))
            {
                nearest = p;
            }
        }
    }

    return nearest;
}

float MasterControl::Sine(const float freq, const float min, const float max, const float shift)
{
    float phase{ SinePhase(freq, shift) };
    float add{ .5f * (min + max) };

    return Sin(ToDegrees(phase)) * .5f * (max - min) + add;
}

float MasterControl::Cosine(const float freq, const float min, const float max, const float shift)
{
    return Sine(freq, min, max, shift + .25f);
}

void MasterControl::SetGameState(GameState state)
{
    if (state == gameState_)
        return;

    gameState_ = state;

    tempMessage_->SetVisible((gameState_ == GS_WIN || gameState_ == GS_LOSE) && level_ < static_cast<int>(levels_.Size()) - 1);

    const String inputString{ INPUT->GetNumJoysticks() == 0u ? "ENTER" : "START" };

    if (gameState_ == GS_WIN)
        tempMessage_->SetText("Level complete!\nPRESS " + inputString);
    else if (gameState_ == GS_LOSE)
        tempMessage_->SetText("Out of time\nPRESS " + inputString);
}

float MasterControl::SinePhase(float freq, float shift)
{
    return M_TAU * (freq * scene_->GetElapsedTime() + shift);
}
