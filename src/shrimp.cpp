/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "urho.h"
#include "spawnmaster.h"
#include "ui/gui3d.h"

#include "shrimp.h"

StaticModelGroup* Shrimp::shrimpGroup_{ nullptr };

Shrimp::Shrimp(Context* context): SceneObject(context),
    modelNode_{ nullptr },
    clockwise_{},
    shrink_{ false }
{
}

void Shrimp::OnNodeSet(Node* node)
{
    if (!node)
        return;

    if (!shrimpGroup_)
        CreateStaticModelGroup();

    modelNode_ = node_->CreateChild("Model");
    modelNode_->SetPosition((Random(2) ? Vector3::RIGHT : Vector3::LEFT) * Random(.1f, .23f) + Vector3::UP * .17f);
    modelNode_->Rotate({ Random(-5.f, 23.f), Vector3::RIGHT });
    clockwise_ = modelNode_->GetDirection().CrossProduct(modelNode_->GetPosition()).y_ > 0.f;
    modelNode_->SetScale(Random(.9, 1.1f));
    modelNode_->RotateAround(Vector3::ZERO, Quaternion{ Random(-5.f, 5.f), Random(360.f), Random(-5.f, 5.f) }, TS_PARENT);
}

void Shrimp::CreateStaticModelGroup()
{
    shrimpGroup_ = node_->GetScene()->CreateComponent<StaticModelGroup>();
    shrimpGroup_->SetModel(CACHE->GetResource<Model>("Models/Shrimp.mdl"));
    shrimpGroup_->SetMaterial(CACHE->GetResource<Material>("Materials/VColHighSpec.xml"));
    shrimpGroup_->SetCastShadows(true);
}

void Shrimp::Set(const Vector3& position, const Quaternion& rotation)
{
    SceneObject::Set(position, rotation);

    shrimpGroup_->AddInstanceNode(modelNode_);
    node_->SetScale(1.f);
    shrink_ = false;
}

void Shrimp::Disable()
{
    SceneObject::Disable();

    shrimpGroup_->RemoveInstanceNode(node_);
}

void Shrimp::Update(float timeStep)
{
    modelNode_->RotateAround(Vector3::ZERO, Quaternion((clockwise_ ? timeStep : -timeStep) * (23.f + 5.f * randomizer_), Vector3::UP), TS_PARENT);

    float distance{ M_INFINITY };
    Urho* nearest{ nullptr };
    for (Urho* urho: Urho::GetUrhos())
    {
        if (!urho)
            continue;

        const float d{ urho->GetWorldPosition().DistanceToPoint(node_->GetWorldPosition()) };
        if (d < distance)
        {
            distance = d;
            nearest = urho;
        }
    }

    if (!shrink_ && distance < M_1_SQRT2)
    {
        Eaten(nearest);
    }
    else if (shrink_)
    {
        float scale{ node_->GetScale().x_ - timeStep * 2.3f };

        if (scale < 0.f)
            Vanish();
        else
            node_->SetScale(scale);
    }

    /// Cheat
//    if (INPUT->GetKeyPress(KEY_DELETE) && INPUT->GetQualifierDown(QUAL_SHIFT))
//        shrink_ = true;
}

void Shrimp::Eaten(Urho* by)
{
    PlaySample(CACHE->GetResource<Sound>("Samples/Crack.ogg"), .8f);
    shrink_ = true;

    GUI3D* gui3d{ GetSubsystem<GUI3D>() };
    gui3d->AddPoints(Max(1, PowN(RoundToInt(by->GetSpeed() - 1.f), 2) - 2), by == Urho::GetUrho());
}

void Shrimp::Vanish()
{
    if (LastShrimp())
        MC->Winner();

    Disable();
}

bool Shrimp::LastShrimp()
{
    return GetSubsystem<SpawnMaster>()->CountActive<Shrimp>() == 1;
}

