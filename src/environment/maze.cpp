/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../spawnmaster.h"
#include "../urho.h"
#include "../shrimp.h"
#include "../hazards/barrelfactory.h"
#include "../hazards/jelly.h"
#include "../hazards/crab.h"
#include "../hazards/anglerfish.h"
#include "../hazards/urchin.h"
#include "block.h"
#include "weed.h"
#include "portal.h"
#include "colourship.h"
#include "../ui/gui3d.h"

#include "maze.h"

Maze::Maze(Context* context): LogicComponent(context),
    mapName_{},
    mapSize_{},
    blockSize_{},
    obstacles_{},
    portals_{},
    pits_{}
{
}

void Maze::OnNodeSet(Node* node)
{
    if (!node)
        return;
}

void Maze::Clear()
{
    node_->RemoveAllChildren();
    obstacles_.Clear();
    portals_.Clear();
    pits_.Clear();

    GetSubsystem<SpawnMaster>()->Clear();
    Urho::ResetPointers();
}

void Maze::LoadBlockMap(const String& fileName)
{
    if (!node_)
        return;

    Clear();

    SpawnMaster* spawn{ GetSubsystem<SpawnMaster>() };
    SharedPtr<XMLFile> mapXML{ RES(XMLFile, fileName) };

    mapName_ = GetFileName(fileName);
    if (IsDigit(mapName_.Front()))
        mapName_ = mapName_.Substring(mapName_.Find('_') + 1);
    if (mapName_.Contains('_'))
        mapName_ = mapName_.Substring(0, mapName_.FindLast('_'));
    if (mapName_.Contains('_'))
        mapName_.Replace('_', ' ');

    Log::Write(LOG_INFO, "Loaded: " + mapName_);

    XMLElement mapElem{ mapXML->GetRoot("blockmap") };
    mapSize_ = mapElem.GetIntVector3("map_size");
    blockSize_ = mapElem.GetVector3("block_size");

    HashMap<int, HashMap<int, Pair<String, String>>> blockSets;
    XMLElement blockSetRefElem{ mapElem.GetChild("blockset") };
    while(blockSetRefElem)
    {
        const int blockSetId{ blockSetRefElem.GetInt("id") };

        SharedPtr<XMLFile> blockSetXML{ RES(XMLFile, blockSetRefElem.GetAttribute("name")) };
        XMLElement blockSetElem{ blockSetXML->GetRoot("blockset") };

        XMLElement blockElem{ blockSetElem.GetChild("block") };
        while (blockElem)
        {
            blockSets[blockSetId][blockElem.GetInt("id")].first_  = blockElem.GetAttribute("model");
            blockSets[blockSetId][blockElem.GetInt("id")].second_ = blockElem.GetAttribute("material");

            blockElem = blockElem.GetNext("block");
        }

        blockSetRefElem = blockSetRefElem.GetNext("blockset");
    }

    // Create and fill obstacles coords set, fill visible area beyond map boundaries with rocks.
    FillVoid();

    XMLElement layerElem{ mapElem.GetChild("gridlayer") };
    while (layerElem)
    {
        const int layerId{ layerElem.GetInt("id") };
        XMLElement blockElem{ layerElem.GetChild("gridblock") };

        while (blockElem)
        {
            const IntVector3 coords{ blockElem.GetIntVector3("coords") };
            const Vector3 position{ CoordsToPosition(coords) };
            const Quaternion rotation{ blockElem.GetQuaternion("rot") };
            const String modelName{ blockSets[blockElem.GetInt("set")][blockElem.GetInt("block")].first_ };
            const String materialName{ blockSets[blockElem.GetInt("set")][blockElem.GetInt("block")].second_ };

            if (coords.y_ == 0)
                obstacles_.Erase({ coords.x_, coords.z_ });

            if (modelName.Contains("Urho"))
            {
                for (int u{ 0 }; u < 1 + MC->IsMultiplayer(); ++u)
                    spawn->Create<Urho>()->Set(position, rotation);
            }
            else if (modelName.Contains("Shrimp"))
                spawn->Create<Shrimp>()->Set(position, rotation);
            else if (modelName.Contains("Barrel"))
                spawn->Create<BarrelFactory>()->Set(position, rotation);
            else if (modelName.Contains("Jelly"))
                spawn->Create<Jelly>()->Set(position, rotation);
            else if (modelName.Contains("Crab"))
                spawn->Create<Crab>()->Set(position, rotation);
            else if (modelName.Contains("Anglerfish"))
                spawn->Create<Anglerfish>()->Set(position, rotation);
            else if (modelName.Contains("Urchin"))
                spawn->Create<Urchin>()->Set(position, rotation);
            else if (modelName.Contains("Portal"))
            {
                Portal* portal{ spawn->Create<Portal>() };
                portal->Set(position, rotation);
                portal->GetNode()->SetParent(node_);
                portal->Match(layerId);
                portals_.Insert({ { coords.x_, coords.z_ }, portal });
            }
            else if (modelName.Contains("Colourship"))
            {
                if (GetSubsystem<GUI3D>()->GetHighestScore() >= 5000u)
                {
                    spawn->Create<Colourship>()->Set(position, rotation);
                    spawn->Create<BarrelFactory>()->Set(position + Vector3{ 2, -5, 2 }, Quaternion{ 90, Vector3::UP } * rotation);
                }
            }
            else
            {
                Node* blockNode{ node_->CreateChild("Block") };
                blockNode->SetPosition(position);
                blockNode->SetRotation(rotation);

                Block* block{ blockNode->CreateComponent<Block>() };
                block->Initialize(RES(Model, modelName),
                                  RES(Material, materialName));

                if (modelName.Contains("Pit"))
                    pits_.Insert(position);

                if (modelName.Contains("PathStraight"))
                    if (Random(3))
                        for (int w{ Random(8) }; w > 0; --w)
                            spawn->Create<Weed>()->Set(position + blockNode->GetRight() * Random(1.f, 1.1f) * (Random(2) ?  .5f : -.5f) + blockNode->GetUp() * .75f + blockNode->GetDirection() * (-.5f + Random()));
            }

            blockElem = blockElem.GetNext("gridblock");
        }

        layerElem = layerElem.GetNext("gridlayer");
    }

    for (Anglerfish* a: spawn->GetActive<Anglerfish>())
        a->MapTerritory();
}

void Maze::FillVoid()
{
    for (int x{ -10 }; x < mapSize_.x_ + 10; ++x)
    for (int y{ -10 }; y < mapSize_.z_ + 10; ++y)
    {
        if (x >= -1 && x <= mapSize_.x_ &&
            y >= -1 && y <= mapSize_.z_)
        {
            obstacles_.Insert({ x, y });
        }

        if (x < 0 || x >= mapSize_.x_ ||
            y < 0 || y >= mapSize_.z_)
        {
            Node* blockNode{ node_->CreateChild("Block") };
            blockNode->SetPosition(CoordsToPosition({ x, 1, y }));
            blockNode->Yaw(Random(4) * 90.f);

            Block* block{ blockNode->CreateComponent<Block>() };
            block->Initialize(RES(Model, "Models/Rock.mdl"),
                              RES(Material, "Materials/VCol.xml"));
        }
    }
}

IntVector3 Maze::WorldPositionToCoords(const Vector3& position) const
{
    return VectorRoundToInt((position - .5f * blockSize_) / blockSize_ + Vector3{ mapSize_ * IntVector3{ 1, 0, 1 } } / 2.f);
}

Vector3 Maze::CoordsToPosition(const IntVector3& coords) const
{
    return { (coords - Vector3{ mapSize_ * IntVector3{ 1, 0, 1 } } / 2.f) * blockSize_ + .5f * blockSize_ };
}

bool Maze::IsObstacle(const Vector3& position)
{
    return IsObstacle(WorldPositionToCoords(position));
}

bool Maze::IsObstacle(const IntVector3& coords)
{
    return obstacles_.Contains({ coords.x_, coords.z_ });
}

HashSet<IntVector2> Maze::GetFreeSquares() const
{
    HashSet<IntVector2> free{};

    for (int x{ 0 }; x < mapSize_.x_; ++x)
    for (int y{ 0 }; y < mapSize_.z_; ++y)
    {
        const IntVector2 coords{ x, y };
        if (!obstacles_.Contains(coords))
            free.Insert(coords);
    }

    return free;
}

bool Maze::IsPortal(const Vector3& position, Urho* urho)
{
    const IntVector3 coords{ WorldPositionToCoords(position) };
    const IntVector2 flatCoords{ coords.x_, coords.z_ };
    return portals_.Contains({ flatCoords }) && portals_[flatCoords]->GetOther() && !portals_[flatCoords]->IsIgnoring(urho);
}

Portal* Maze::GetPortal(const Vector3& position)
{
    const IntVector3 coords{ WorldPositionToCoords(position) };
    return portals_[{ coords.x_, coords.z_ }];
}

const CameraProperties Maze::GetCameraProperties()
{
    const float windowRatio{ GetSubsystem<Graphics>()->GetRatio() };
    const float verticalDistance{ static_cast<float>(mapSize_.z_) };
    const float horizontalDistance{ static_cast<float>(mapSize_.x_) / windowRatio };
    const float distance{ Max(verticalDistance, horizontalDistance) * 1.05f };

    return { Vector3{ .0f, 23.5f, -4.2f }.Normalized() * distance,
             Vector3::DOWN,
             53.0f };
}

Vector3 Maze::MapSizeModulo() const
{
    return Vector3(mapSize_.x_ % 2, mapSize_.y_ % 2, mapSize_.z_ % 2);
}
