/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MAZE_H
#define MAZE_H

#include "../mastercontrol.h"

class Urho;
class Portal;

class Maze: public LogicComponent
{
    DRY_OBJECT(Maze, LogicComponent);

public:
    Maze(Context* context);
    void OnNodeSet(Node* node) override;
    void LoadBlockMap(const String& fileName);

    IntVector3 GetSize() const { return mapSize_; }
    float GetRatio() const { return 1.f * mapSize_.x_ / mapSize_.z_; }
    const HashSet<Vector3>& GetPits() const { return pits_; }
    bool IsObstacle(const Vector3& position);
    bool IsObstacle(const IntVector3& coords);
    HashSet<IntVector2> GetFreeSquares() const;
    bool IsPortal(const Vector3& position, Urho* urho);
    Portal* GetPortal(const Vector3& position);

    Vector3 CoordsToPosition(const IntVector3& coords) const;
    IntVector3 WorldPositionToCoords(const Vector3& position) const;
    
    const CameraProperties GetCameraProperties();
    void Clear();

private:
    Vector3 MapSizeModulo() const;
    void FillVoid();

    String mapName_;
    IntVector3 mapSize_;
    Vector3 blockSize_;
    HashSet<IntVector2> obstacles_;
    HashMap<IntVector2, Portal*> portals_;
    HashSet<Vector3> pits_;
};

#endif // MAZE_H
