/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../urho.h"

#include "colourship.h"

Colourship::Colourship(Context* context): SceneObject(context)
{
}

void Colourship::OnNodeSet(Node* node)
{
    if (!node)
        return;

    StaticModel* ship{ node_->CreateComponent<StaticModel>() };
    ship->SetModel(RES(Model, "Models/Colourship.mdl"));
    ship->SetMaterial(RES(Material, "Materials/Colourship.xml"));
    ship->SetCastShadows(true);
    ship->SetLightMask(1u);
    ship->SetShadowMask(1u);

    Node* lightsNode{ node_->CreateChild("Lights") };
    lightsNode->Yaw(-135.f);
    Node* bridgeNode{ lightsNode->CreateChild("Bridge") };
    bridgeNode->Translate({ 0.f, 1.5f, 3.f });
    Light* bridgeLight{ bridgeNode->CreateComponent<Light>() };
    bridgeLight->SetColor(Color::YELLOW);
    Node* boomNode{ lightsNode->CreateChild("Boom") };
    boomNode->Translate({ 2.f, 1.5f, -1.f });
    Light* boomLight{ boomNode->CreateComponent<Light>() };
    boomLight->SetColor(Color::BLUE);
    Node* brickNode{ lightsNode->CreateChild("Brick") };
    brickNode->Translate({ -2.f, 1.5f, -1.f });
    Light* brickLight{ brickNode->CreateComponent<Light>() };
    brickLight->SetColor(Color::RED);
    Node* messNode{ lightsNode->CreateChild("Brick") };
    messNode->Translate({ 0.f, 2.5f, -1.f });
    Light* messLight{ messNode->CreateComponent<Light>() };
    messLight->SetColor(Color::GREEN);

    for (Light* light: { bridgeLight, boomLight, brickLight, messLight })
    {
        light->SetBrightness(.5f);
        light->SetRange(5.f);
        light->SetLightMask(M_MAX_UNSIGNED - 1u);
        light->SetShadowMask(M_MAX_UNSIGNED - 1u);
        light->SetCastShadows(true);
        light->SetShadowResolution(.125f);
    }

    for (Node* deckNode: { bridgeNode, boomNode, brickNode })
    {
        deckNode->CreateComponent<RigidBody>();
        CollisionShape* shape{ deckNode->CreateComponent<CollisionShape>() };
        shape->SetConvexHull(RES(Model, "Models/DeckCollider.mdl"));
    }
}

void Colourship::Update(float /*timeStep*/)
{
    for (bool first: { true, false })
    {
        Urho* urho{ Urho::GetUrho(first) };

        PODVector<RigidBody*> bodies{};
        const bool hit{ GetScene()->GetComponent<PhysicsWorld>()->GetRigidBodies(bodies, { urho->GetNode()->GetWorldPosition(), .11f } ) };
        if (hit)
        {
            //        if (FILES->SystemRun("exo-open", { "--launch", "WebBrowser", "https://matrix.to/#/#lkp-colourship:matrix.org" }) != -1)
            MC->ErrorExit("Join us!\n"
                          "https://com.luckey.games\n"
                          "https://matrix.to/#/#lkp-colourship:matrix.org");
        }
    }
}
