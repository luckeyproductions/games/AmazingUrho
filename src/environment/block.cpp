/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "block.h"

HashMap<StringHash, StaticModelGroup*> Block::blockGroups_{};

Block::Block(Context* context): Component(context)
{

}

void Block::OnNodeSet(Node* node)
{ (void)node;

//    node_->CreateComponent<StaticModel>();

//    RigidBody* rigidBody{ node_->CreateComponent<RigidBody>() };
//    rigidBody->SetCollisionLayer(LAYER(0));
//    rigidBody->SetFriction(0.8f);
//    collider_ = node_->CreateComponent<CollisionShape>();
}

void Block::Initialize(Model* model, Material* material)
{
//    node_->GetComponent<StaticModel>()->SetModel(model);
//    node_->GetComponent<StaticModel>()->

    StringHash modelNameHash{ model->GetNameHash() };

    if (!blockGroups_.Contains(modelNameHash)) {

        blockGroups_[modelNameHash] = node_->GetScene()->CreateComponent<StaticModelGroup>();
        blockGroups_[modelNameHash]->SetModel(model);
        blockGroups_[modelNameHash]->SetCastShadows(true);
        blockGroups_[modelNameHash]->SetMaterial(material);
    }

    blockGroups_[modelNameHash]->AddInstanceNode(node_);
}

void Block::Update(float timeStep)
{
}




