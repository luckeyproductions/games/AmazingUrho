/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PORTAL_H
#define PORTAL_H

#include "../sceneobject.h"

class Urho;

class Portal: public SceneObject
{
    DRY_OBJECT(Portal, SceneObject);

public:
    Portal(Context* context);
    void Set(const Vector3& position, const Quaternion& rotation) override;

    void Match(int layer);
    int GetLayer() const { return layer_; }
    void SetOther(Portal* portal) { other_ = portal; }
    Portal* GetOther() const { return other_; }
    void EnableIgnore(Urho* urho) { ignore_.Insert(urho); }
    bool IsIgnoring(Urho* urho) const { return ignore_.Contains(urho); }
    Vector3 GetOffset(Urho* urho) const;
    void Update(float timeStep) override;

protected:
    void OnNodeSet(Node* node) override;

private:
    Portal* other_;
    int layer_;
    HashSet<Urho*> ignore_;
};

#endif // PORTAL_H
