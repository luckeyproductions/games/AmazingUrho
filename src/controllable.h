/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef CONTROLLABLE_H
#define CONTROLLABLE_H

#include <bitset>

#include "sceneobject.h"

#define INPUTMASTER GetSubsystem<InputMaster>()

class Controllable: public SceneObject
{
    friend class InputMaster;
    DRY_OBJECT(Controllable, SceneObject);
public:
    Controllable(Context* context);
    void Update(float timeStep) override;

    void SetMove(Vector3 move);
    void SetAim(Vector3 aim);
    virtual void ClearControl();
    virtual void Think(float timeStep) {}

    Vector3 GetLinearVelocity() const { return rigidBody_->GetLinearVelocity(); }
    Player* GetPlayer();

protected:
    void OnNodeSet(Node* node) override;

    void ResetInput() { move_ = aim_ = Vector3::ZERO; actions_.reset(); }
    void SetActions(std::bitset<4> actions);
    virtual void HandleAction(int actionId);
    void ClampPitch(Quaternion& rot);

    void AlignWithVelocity(float timeStep);
    void AlignWithMovement(float timeStep);

private:
    bool controlled_;
    Vector3 move_;
    Vector3 aim_;
    float thrust_;
    float maxSpeed_;
    float maxPitch_;
    float minPitch_;

    std::bitset<4> actions_;
    HashMap<int, float> actionSince_;
    HashMap<int, float> actionInterval_;

    AnimatedModel* model_;
    RigidBody* rigidBody_;
    CollisionShape* collisionShape_;
    AnimationController* animCtrl_;
};

#endif // CONTROLLABLE_H
