/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "environment/maze.h"
#include "environment/portal.h"
#include "ui/gui3d.h"

#include "urho.h"

Pair<Urho*, Urho*> Urho::urhos_{};

Urho::Urho(Context* context): SceneObject(context),
    startPosition_{},
    startRotation_{},
    targetPosition_{},
    speed_{ SPEED_MIN },
    graphicsNode_{ nullptr },
    offsetNode_{ nullptr }
{
}

void Urho::OnNodeSet(Node* node)
{
    if (!node)
        return;

    if (!urhos_.first_)
        urhos_.first_ = this;
    else
        urhos_.second_ = this;

    graphicsNode_ = node_->CreateChild("UrhoModel");
    graphicsNode_->SetScale({ 1.23f, 1.f, 1.f });
    graphicsNode_->SetPosition(Vector3::BACK * .1f);
    offsetNode_ = graphicsNode_->CreateChild("Offset");
    AnimatedModel* urhoModel{ offsetNode_->CreateComponent<AnimatedModel>() };
    urhoModel->SetModel(CACHE->GetResource<Model>("Models/Urho.mdl"));
    urhoModel->SetCastShadows(true);
    urhoModel->ApplyMaterialList();
    node_->CreateComponent<AnimationController>()->PlayExclusive("Models/Swim.ani", 0u, true);

    if (this == urhos_.second_)
    {
        SharedPtr<Material> mat{ urhoModel->GetMaterial()->Clone() };
        mat->SetShaderParameter("MatDiffColor", Color{ .75, .9, 1 });
        mat->SetShaderParameter("MatEmissiveColor", Color{ 1, .23, .23 });
        urhoModel->SetMaterial(mat);
    }
}

void Urho::Set(const Vector3& position, const Quaternion& rotation)
{
    SceneObject::Set(position, rotation);

    targetPosition_ = startPosition_ = node_->GetWorldPosition();
    startRotation_ = rotation;
    speed_ = SPEED_MIN;
}

void Urho::Hit()
{
    speed_ = 0.f;
    Set(startPosition_, startRotation_);

    if (MC->GetGameState() != GS_PLAY)
        return;

    VariantMap eventData{};
    eventData[UrhoHit::P_FISH] = this;
    SendEvent(E_URHOHIT, eventData);
}

void Urho::Jump(const Vector3& shift)
{
    node_->Translate(shift, TS_WORLD);
    targetPosition_ += shift;
}

void Urho::Update(float timeStep)
{
    const Vector3 toTarget{ targetPosition_ - node_->GetWorldPosition() };
    node_->Translate(toTarget * timeStep * speed_, TS_WORLD);

    if (AnimationController* animCtrl = node_->GetComponent<AnimationController>())
        animCtrl->SetSpeed("Models/Swim.ani", .34f * speed_);

    if (MC->GetGameState() == GS_PLAY && !GetSubsystem<GUI3D>()->IsCountingDown())
    {
        Vector3 newTarget{};
        bool step{      false };
        bool joyUp{     false };
        bool joyLeft{   false };
        bool joyRight{  false };

        JoystickState* joy{ INPUT->GetJoystickByIndex(0u + (this == GetUrho(false))) };
        if (joy)
        {
            joyUp     = joy->GetButtonPress(CONTROLLER_BUTTON_A);
            joyLeft   = joy->GetButtonPress(CONTROLLER_BUTTON_LEFTSHOULDER);
            joyRight  = joy->GetButtonPress(CONTROLLER_BUTTON_RIGHTSHOULDER);
            joyUp    |= joy->GetButtonPress(CONTROLLER_BUTTON_DPAD_UP);
            joyLeft  |= joy->GetButtonPress(CONTROLLER_BUTTON_DPAD_LEFT);
            joyRight |= joy->GetButtonPress(CONTROLLER_BUTTON_DPAD_RIGHT);
        }

        bool keyUp{ false };
        bool keyLeft{ false };
        bool keyRight{ false };

        if (!MC->IsMultiplayer() || this == GetUrho(true))
        {
            keyUp    |= INPUT->GetKeyPress(KEY_W);
            keyLeft  |= INPUT->GetKeyPress(KEY_A);
            keyRight |= INPUT->GetKeyPress(KEY_D);
        }

        if (!MC->IsMultiplayer() || this == GetUrho(false))
        {
            keyUp    |= INPUT->GetKeyPress(KEY_UP);
            keyLeft  |= INPUT->GetKeyPress(KEY_LEFT);
            keyRight |= INPUT->GetKeyPress(KEY_RIGHT);
        }

        if (keyUp || joyUp)
        {
            newTarget = targetPosition_ + node_->GetDirection();
            step = true;
        }
        else if (keyLeft || joyLeft)
        {
            newTarget = targetPosition_ - node_->GetRight();
            step = true;
        }
        else if (keyRight || joyRight)
        {
            newTarget = targetPosition_ + node_->GetRight();
            step = true;
        }

        if (step)
        {
            Maze* maze{ MC->GetMaze() };
            const float distance{ toTarget.Length() };
            Portal* portal{ (maze->IsPortal(targetPosition_, this) ? maze->GetPortal(targetPosition_)
                                                                   : nullptr) };
            if (portal)
                newTarget += portal->GetOffset(this);

            const bool obstacle{ maze->IsObstacle(newTarget)};

            if (distance < .8f && !obstacle)
            {
                float acceleration{ DistanceToAcceleration(distance) };

                if (acceleration < .7f)
                    PlaySample(CACHE->GetResource<Sound>("Samples/Swim0.ogg"), .666f);
                else if (acceleration < 1.4f)
                    PlaySample(CACHE->GetResource<Sound>("Samples/Swim1.ogg"), .666f);
                else if (acceleration < 2.1f)
                    PlaySample(CACHE->GetResource<Sound>("Samples/Swim2.ogg"), .666f);
                else
                    PlaySample(CACHE->GetResource<Sound>("Samples/Swim3.ogg"), .666f);

                speed_ += acceleration;

                const Vector3 oldDirection{graphicsNode_->GetWorldDirection()};
                if (portal)
                {
                    Jump(portal->GetOffset(this));
                    portal->EnableIgnore(this);
                    portal->GetOther()->EnableIgnore(this);
                }

                node_->SetDirection(newTarget - targetPosition_);
                graphicsNode_->SetWorldDirection(oldDirection);
                targetPosition_ = newTarget;


                VariantMap eventData{};
                eventData[UrhoSwam::P_FISH] = this;
                eventData[UrhoSwam::P_SUCCESS] = true;
                SendEvent(E_URHOSWAM, eventData);
            }
            else
            {
                speed_ = Max(SPEED_MIN, speed_ * .666f);

                if (obstacle)
                    PlaySample(CACHE->GetResource<Sound>("Samples/Bump.ogg"), .8f);

                VariantMap eventData{};
                eventData[UrhoSwam::P_FISH] = this;
                eventData[UrhoSwam::P_SUCCESS] = false;
                SendEvent(E_URHOSWAM, eventData);
            }
        }
    }

    speed_ = Lerp(speed_, SPEED_MIN, timeStep * 1.8f);
    graphicsNode_->SetRotation(graphicsNode_->GetRotation().Slerp(Quaternion::IDENTITY, timeStep * speed_ * 2.3f));


    if (MC->IsMultiplayer())
    {
        const float distance{ urhos_.first_->GetNode()->GetWorldPosition().DistanceToPoint(urhos_.second_->GetNode()->GetWorldPosition()) };
        float factor{ Max(0.f, M_SQRT2 - distance) * M_1_SQRT2 };
        if (urhos_.second_ == this)
            factor *= urhos_.first_->graphicsNode_->GetWorldDirection().DotProduct(urhos_.second_->graphicsNode_->GetWorldDirection());

        offsetNode_->SetPosition(factor * Vector3::RIGHT * (.2f - .4f * (this == urhos_.first_)));
    }
}
