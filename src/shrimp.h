/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SHRIMP_H
#define SHRIMP_H


#include "sceneobject.h"


class Shrimp: public SceneObject
{
    DRY_OBJECT(Shrimp, SceneObject);

public:
    Shrimp(Context* context);
    void OnNodeSet(Node* node) override;
    void Update(float timeStep) override;

    void Set(const Vector3& position, const Quaternion& rotation) override;
    void Disable() override;
    void Eaten(Urho* by);
    void Vanish();
    
private:
    void CreateStaticModelGroup();
    bool LastShrimp();

    static StaticModelGroup* shrimpGroup_;
    Node* modelNode_;
    bool clockwise_;
    bool shrink_;
};

#endif // SHRIMP_H
