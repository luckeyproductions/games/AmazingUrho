/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "gui3d.h"

#include "mainmenu.h"

MainMenu::MainMenu(Context* context): LogicComponent(context),
    urhoNode_{ nullptr },
    otherUrhoNode_{ nullptr },
    buttonMaterials_{},
    selectedItem_{ 0 },
    urhoSpeed_{ 0.f }
{
}

void MainMenu::OnNodeSet(Node* node)
{
    if (!node)
        return;

    //Poles
    for (int p{ 0 }; p < 2; ++p)
    {
        Node* poleNode{ node_->CreateChild("Pole") };
        poleNode->SetPosition(Vector3::RIGHT * (1.7f - 3.4f * p) + Vector3::FORWARD * .333f);
        StaticModel* poleModel{ poleNode->CreateComponent<StaticModel>() };
        poleModel->SetModel(CACHE->GetResource<Model>("Models/Pole.mdl"));
        poleModel->SetMaterial(CACHE->GetResource<Material>("Materials/VCol.xml"));
        poleModel->SetCastShadows(true);
    }

    //Planks
    Material* buttonMaterial{ CACHE->GetResource<Material>("Materials/Button.xml") };

    for (int p{ 0 }; p < 3; ++p)
    {
        String text{};
        switch (p)
        {
        case 0: text = "Play";      break;
        case 1: text = "Compete";   break;
        case 2: text = "Exit";      break;
        default: continue;
        }

        Node* plankNode{ node_->CreateChild("Button") };
        StaticModel* plankModel{ plankNode->CreateComponent<StaticModel>() };
        plankModel->SetModel(CACHE->GetResource<Model>("Models/Plank_" + text + ".mdl"));
        plankModel->SetMaterial(0, CACHE->GetResource<Material>("Materials/VCol.xml"));

        plankModel->SetMaterial(1, buttonMaterial->Clone());
        plankModel->SetCastShadows(true);

        buttonMaterials_.Push(plankModel->GetMaterial(1));
    }

    Node* terrainNode{ node_->CreateChild("Terrain") };
    StaticModel* terrainModel{ terrainNode->CreateComponent<StaticModel>() };
    terrainModel->SetModel(CACHE->GetResource<Model>("Models/Terrain.mdl"));
    terrainModel->SetMaterial(CACHE->GetResource<Material>("Materials/VCol.xml"));
    terrainModel->SetCastShadows(true);

    Node* titleNode{ node_->CreateChild("Title") };
    StaticModel* titleModel{ titleNode->CreateComponent<StaticModel>() };
    titleModel->SetModel(CACHE->GetResource<Model>("Models/AmazingUrho.mdl"));
    titleModel->SetMaterial(0, CACHE->GetResource<Material>("Materials/VCol.xml"));
    titleModel->SetMaterial(1, CACHE->GetResource<Material>("Materials/Title.xml"));
    titleModel->SetCastShadows(true);

    urhoNode_ = node_->CreateChild("Urho");
    urhoNode_->SetScale(5.f);
    urhoNode_->SetPosition({ 9.5f, 2.3f, 21.5f });
    urhoNode_->SetRotation({ 160.f, Vector3::UP });
    AnimatedModel* urhoModel{ urhoNode_->CreateComponent<AnimatedModel>() };
    urhoModel->SetModel(CACHE->GetResource<Model>("Models/Urho.mdl"));
    urhoModel->ApplyMaterialList();
    urhoModel->SetCastShadows(true);

    otherUrhoNode_ = urhoNode_->CreateChild("Other");
    otherUrhoNode_->SetPosition({ .42f, -.05f, -1.f });
    AnimatedModel* otherUrhoModel{ otherUrhoNode_->CreateComponent<AnimatedModel>() };
    otherUrhoModel->SetModel(CACHE->GetResource<Model>("Models/Urho.mdl"));
    otherUrhoModel->ApplyMaterialList();
    otherUrhoModel->SetCastShadows(true);
    SharedPtr<Material> mat{ otherUrhoModel->GetMaterial()->Clone() };
    mat->SetShaderParameter("MatDiffColor", Color{ .75, .9, 1 });
    mat->SetShaderParameter("MatEmissiveColor", Color{ 1, .23, .23 });
    otherUrhoModel->SetMaterial(mat);

    urhoNode_->CreateComponent<AnimationController>()->PlayExclusive("Models/Swim.ani", 0, true);
    AnimationController* otherAnimCtrl{ otherUrhoNode_->CreateComponent<AnimationController>() };
    otherAnimCtrl->PlayExclusive("Models/Swim.ani", 0, true);
    otherAnimCtrl->SetTime("Models/Swim.ani", 1/3.f * otherAnimCtrl->GetLength("Models/Swim.ani"));
    otherUrhoNode_->SetEnabled(false);
}

void MainMenu::Update(float timeStep)
{
    urhoNode_->GetComponent<AnimationController>()->SetSpeed("Models/Swim.ani", .17f + urhoSpeed_ * .1f);
    otherUrhoNode_->GetComponent<AnimationController>()->SetSpeed("Models/Swim.ani", .17f + urhoSpeed_ * .1f);

    for (unsigned b{ 0u }; b < buttonMaterials_.Size(); ++b)
    {
        Material* mat{ buttonMaterials_[b] };
        mat->SetShaderParameter("MatEmissiveColor", mat->GetShaderParameter("MatEmissiveColor").GetColor().Lerp(
                                    (static_cast<int>(b) == selectedItem_ ? Color::WHITE : Color::BLACK),
                                    timeStep * 13.f) );
    }

    if (selectedItem_ == -1)
    {
        urhoSpeed_ = Lerp(urhoSpeed_, 23.f, timeStep * (.23f * urhoSpeed_ + .23f));
        urhoNode_->Translate(Vector3::FORWARD * timeStep * urhoSpeed_);
        urhoNode_->Rotate({ -timeStep * 2.3f,
                             timeStep * 4.2f * (10.f - urhoSpeed_ * .8f),
                             0.f });

        if ((!MC->IsMultiplayer() ? !urhoNode_->GetComponent<AnimatedModel>()->IsInView()
                                  : !otherUrhoNode_->GetComponent<AnimatedModel>()->IsInView()))
            MC->StartNewGame();

        return;
    }


    HandleInput();
}

void MainMenu::HandleInput()
{
    bool joyUp     { false };
    bool joyDown   { false };
    bool joyConfirm{ false };
    JoystickState* joy{ INPUT->GetJoystickByIndex(0u) };
    if (joy)
    {
        joyUp       = joy->GetButtonPress(CONTROLLER_BUTTON_DPAD_UP);
        joyDown     = joy->GetButtonPress(CONTROLLER_BUTTON_DPAD_DOWN);
        joyConfirm  = joy->GetButtonPress(CONTROLLER_BUTTON_A);
        joyConfirm |= joy->GetButtonPress(CONTROLLER_BUTTON_START);
    }

    const bool stepUp{ INPUT->GetKeyPress(KEY_UP)   || joyUp };
    const bool stepDown{ INPUT->GetKeyPress(KEY_DOWN) || joyDown };
    const int step{ stepDown - stepUp };

    if (step != 0)
    {
        selectedItem_ = (selectedItem_ + 3 + step) % 3;
        SoundSource* source{ node_->GetOrCreateComponent<SoundSource>() };
        source->SetGain(1.0f);
        source->Play(CACHE->GetResource<Sound>("Samples/Select.ogg"));
    }
    else if (INPUT->GetKeyPress(KEY_RETURN)
          || INPUT->GetKeyPress(KEY_RETURN2)
          || INPUT->GetKeyPress(KEY_KP_ENTER)
          || joyConfirm)
    {
        switch (selectedItem_)
        {
        case 0: MC->SetCompete(false); otherUrhoNode_->SetEnabled(false); selectedItem_ = -1; break;
        case 1: MC->SetCompete(true);  otherUrhoNode_->SetEnabled(true); selectedItem_ = -1; break;
        case 2: MC->Exit();                                break;
        default: break;
        }
    }
}

const CameraProperties MainMenu::GetCameraProperties()
{
    return { { -4.2f, 3.4f, -17.f  },
             {  4.5f, 4.2f,  -2.3f },
                42.f };
}

void MainMenu::SetEnabled(bool enable)
{
    node_->SetEnabledRecursive(enable);
    SetUpdateEventMask((enable ? USE_UPDATE : USE_NO_EVENT));

    Component::SetEnabled(enable);
}



