/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef GUI3D_H
#define GUI3D_H

#include "../luckey.h"

#define LOBBYPOS Vector3::FORWARD * 6.42f

class GUI3D: public LogicComponent
{
    DRY_OBJECT(GUI3D, LogicComponent);

public:
    static void RegisterObject(Context* context);
    GUI3D(Context* context);

    void StartLevel(unsigned seconds);
    void RestartLevel();

    void SetTime(unsigned seconds);
    void UrhoHit(bool urhoOne);

    bool IsCountingDown() const;

    void Sync();

    void AddPoints(unsigned points, bool first = true);
    unsigned GetHighestScore() const;
    
protected:
    void OnNodeSet(Node* node) override;
    void Update(float timeStep) override;
    void OnSetEnabled() override;

private:
    void CreateScore();
    void CreateClock();
    void CreateSpeedometer();
    void CountScore();
    void Countdown();

    void HandleDigitRisen(StringHash eventType, VariantMap& eventData);
    void HandleScreenMode(StringHash eventType, VariantMap& eventData);

    unsigned toCount_;
    unsigned time_;
    unsigned startTime_;
    float second_;

    Node* timeNode_;
    HashMap<int, Node*> clockDigits_;
    Pair<Node*, Node*> scoreNodes_;
    Pair<Node*, Node*> speedoNodes_;
};

DRY_EVENT(E_RISEN, Risen)
{
    DRY_PARAM(P_NODE, Node);
}

#endif // GUI3D_H
