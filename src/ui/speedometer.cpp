/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../urho.h"

#include "speedometer.h"

Speedometer::Speedometer(Context* context): LogicComponent(context),
    urhoOne_{ true },
    glowMats_{}
{
}

void Speedometer::Update(float timeStep)
{
    Node* handNode{ node_->GetChild("Hand") };
    if (Urho* urho{ GetUrho() })
    {
        handNode->SetRotation(handNode->GetRotation().Slerp({ 15.f + Min(150.f, 15.f * urho->GetSpeed()), Vector3::BACK }, timeStep * 17.f));
        glowMats_.first_->SetShaderParameter("MatEmissiveColor", AccelerationToColor(urho->DistanceToAcceleration(urho->GetDistance())));
    }
}

void Speedometer::SetUrhoTwo()
{
    urhoOne_ = false;

    UnsubscribeFromEvents(Urho::GetUrho());

    SubscribeToEvents();
}

void Speedometer::SubscribeToEvents()
{
    SubscribeToEvent(E_URHOSWAM, DRY_HANDLER(Speedometer, HandleUrhoSwam));
    SubscribeToEvent(E_URHOHIT,  DRY_HANDLER(Speedometer, HandleUrhoHit));
}

void Speedometer::OnNodeSet(Node* node)
{
    if (!node)
        return;

    StaticModel* speedoCase{ node_->CreateComponent<StaticModel>() };
    speedoCase->SetModel(RES(Model, "Models/SpeedoCase.mdl"));
    speedoCase->SetCastShadows(true);
    Node* speedoHandNode{ node_->CreateChild("Hand") };
    StaticModel* speedoHand{ speedoHandNode->CreateComponent<StaticModel>() };
    speedoHand->SetModel(RES(Model, "Models/SpeedoHand.mdl"));
    speedoHandNode->SetRotation({ 15.f, Vector3::FORWARD });

    for (StaticModel* model: { speedoCase, speedoHand })
    {
        model->SetMaterial(0u, RES(Material, "Materials/VColHighSpec.xml"));
        SharedPtr<Material> glowMat{ RES(Material, "Materials/Glow.xml")->Clone(model->GetNode()->GetName() + "Glow") };
        model->SetMaterial(1u, glowMat);
        glowMat->SetShaderParameter("MatEmissiveColor", Color::BLACK);

        if (model == speedoHand)
            glowMats_.first_ = glowMat;
        else
            glowMats_.second_ = glowMat;
    }

    SubscribeToEvents();
}

Urho* Speedometer::GetUrho()
{
    return Urho::GetUrho(urhoOne_);
}

Color Speedometer::AccelerationToColor(float acceleration)
{
    const float h{ acceleration * .125f };
    const float s{ 1.f };
    const float v{ Clamp(acceleration * 10.f, 0.f, 1.f) };

    Color color{ Color::BLACK };
    color.FromHSV(h, s, v);
    return color;
}

void Speedometer::HandleUrhoSwam(StringHash eventType, VariantMap& eventData)
{
    if (GetUrho() != static_cast<Urho*>(eventData[UrhoHit::P_FISH].GetPtr()))
        return;

    if (eventData[UrhoSwam::P_SUCCESS].GetBool())
        glowMats_.second_->SetShaderParameter("MatEmissiveColor", glowMats_.first_->GetShaderParameter("MatEmissiveColor"));
    else
        glowMats_.second_->SetShaderParameter("MatEmissiveColor", Color::RED);

    ValueAnimation* fade{ new ValueAnimation{ context_ } };
    fade->SetKeyFrame(0.f, glowMats_.second_->GetShaderParameter("MatEmissiveColor").GetColor());
    fade->SetKeyFrame(2/3.f, Color::BLACK);
    fade->SetInterpolationMethod(IM_SINUSOIDAL);
    glowMats_.second_->SetShaderParameterAnimation("MatEmissiveColor", fade, WM_CLAMP);
}

void Speedometer::HandleUrhoHit(StringHash eventType, VariantMap& eventData)
{
    if (GetUrho() != static_cast<Urho*>(eventData[UrhoHit::P_FISH].GetPtr()))
        return;

    glowMats_.second_->SetShaderParameter("MatEmissiveColor", Color::BLACK);
}
