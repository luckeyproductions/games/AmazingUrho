/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../urho.h"

#include "scoreboard.h"

Scoreboard::Scoreboard(Context* context): LogicComponent(context),
    scoreDigits_{},
    score_{ 0u },
    startScore_{ score_ },
    urhoOne_{ true }
{
}

void Scoreboard::OnNodeSet(Node* node)
{
    if (!node)
        return;

    for (int d{ 0 }; d < 10; ++d)
    {
        scoreDigits_[d] = node_->CreateChild("Digit");
        scoreDigits_[d]->SetEnabled(d == 0);
        scoreDigits_[d]->Translate(Vector3::LEFT * (d - 2.5f), TS_WORLD);

        StaticModel* digitModel{ scoreDigits_[d]->CreateComponent<StaticModel>() };
        digitModel->SetModel(RES(Model, "Models/0.mdl"));
        digitModel->SetMaterial(RES(Material, "Materials/Bubble.xml"));
        digitModel->SetCastShadows(true);
    }

    SubscribeToEvents();
}

void Scoreboard::Update(float timeStep)
{
    for (int d{ 0 }; d < 10; ++d)
    {
        const float et{ GetScene()->GetElapsedTime() };
        Node* digit{ scoreDigits_[d] };

        if (digit)
        {
            digit->SetScale(.55f + Sin(et * 192.f + d * 23.f) * .034f);
            digit->SetRotation({ 5.f * Cos(et * (101.f - d * 5.f) + d * 128.f), Vector3::RIGHT });
        }
    }
}

void Scoreboard::SetUrhoTwo()
{
    urhoOne_ = false;

    UnsubscribeFromEvents(Urho::GetUrho());
    SubscribeToEvents();
}

void Scoreboard::SubscribeToEvents()
{
    SubscribeToEvent(E_URHOHIT,  DRY_HANDLER(Scoreboard, HandleUrhoHit));
}

void Scoreboard::SetScore(unsigned score)
{
    score_ = score;

    //Update score graphics
    for (unsigned d{ 0u }; d < 10u; ++d)
    {
        const unsigned p{ PowN(10u, d) };
        StaticModel* digitModel{ scoreDigits_[d]->GetComponent<StaticModel>() };
        digitModel->SetModel(RES(Model, "Models/" + String((score_ / p) % 10u ) + ".mdl"));
        scoreDigits_[d]->SetEnabled(score_ >= p || d == 0);
    }
}

void Scoreboard::AddPoints(unsigned points)
{
    SetScore(score_ + points);
}

void Scoreboard::Penalty(unsigned points)
{
    SetScore(score_ - Min(score_, points));
}

void Scoreboard::HandleUrhoHit(StringHash eventType, VariantMap& eventData)
{
    Penalty(100u);
}
