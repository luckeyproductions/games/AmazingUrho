/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../mastercontrol.h"
#include "../spawnmaster.h"
#include "../effectmaster.h"
#include "../player.h"
#include "../urho.h"
#include "../environment/maze.h"

#include "scoreboard.h"
#include "speedometer.h"

#include "gui3d.h"

void GUI3D::RegisterObject(Context* context)
{
    context->RegisterFactory<GUI3D>();
    context->RegisterFactory<Scoreboard>();
    context->RegisterFactory<Speedometer>();
}

GUI3D::GUI3D(Context* context): LogicComponent(context),
    toCount_{ 0u },
    time_{ 0u },
    startTime_{ time_ },
    second_{ 0.f },
    timeNode_{ nullptr },
    clockDigits_{},
    scoreNodes_{},
    speedoNodes_{}
{
}

void GUI3D::StartLevel(unsigned seconds)
{
    startTime_ = seconds;

    SetEnabled(false); ///
    SetTime(seconds);
    timeNode_->SetPosition(timeNode_->GetPosition() * Vector3{ !MC->IsMultiplayer() * 1.f, 1.f, 1.f });
    for (bool first: { true, false })
    {
        Node* scoreNode{ (first ? scoreNodes_.first_ : scoreNodes_.second_) };
        Scoreboard* board{ scoreNode->GetComponent<Scoreboard>() };
        board->StartLevel();
    }
    SetEnabled(true);

    scoreNodes_.second_->SetEnabledRecursive(MC->IsMultiplayer());
    speedoNodes_.second_->SetEnabledRecursive(MC->IsMultiplayer());

    Sync();
}

void GUI3D::RestartLevel()
{

    SetEnabled(false); ///
    SetTime(startTime_);
    for (bool first: { true, false })
    {
        Node* scoreNode{ (first ? scoreNodes_.first_ : scoreNodes_.second_) };
        Scoreboard* board{ scoreNode->GetComponent<Scoreboard>() };
        board->RestartLevel();
    }
    SetEnabled(true);

    speedoNodes_.second_->SetEnabledRecursive(MC->IsMultiplayer());
}

void GUI3D::OnNodeSet(Node* node)
{
    if (!node)
        return;

    node_->LookAt(node_->GetWorldPosition() + Vector3::DOWN, Vector3::FORWARD);

    CreateScore();
    CreateClock();
    CreateSpeedometer();
}

void GUI3D::CreateScore()
{
    for (bool first: { true, false })
    {
        Node* scoreNode{ node_->CreateChild("Score") };
        Scoreboard* board{ scoreNode->CreateComponent<Scoreboard>() };
        if (!first)
            board->SetUrhoTwo();

        scoreNode->Rotate({ -5.f, Vector3::RIGHT });
        scoreNode->SetScale(.42f);
        scoreNode->Translate({ -5.5f * (1 - (2 * !first)), 0.f, 4.2f }, TS_WORLD);

        if (first)
            scoreNodes_.first_  = scoreNode;
        else
            scoreNodes_.second_ = scoreNode;
    }
}

void GUI3D::CreateClock()
{
    timeNode_ = node_->CreateChild("Clock");

    for (int d{ 0 }; d < 5; ++d)
    {
        clockDigits_[d] = timeNode_->CreateChild("Digit");
        clockDigits_[d]->SetEnabled(d < 4);
        clockDigits_[d]->SetScale(.55f);
        clockDigits_[d]->Translate(Vector3::LEFT * (d - .23f * ((d >= 2) + (d >= 3))), TS_WORLD);

        StaticModel* digitModel{ clockDigits_[d]->CreateComponent<StaticModel>() };
        if (d == 2)
            digitModel->SetModel(RES(Model, "Models/:.mdl"));
        else
            digitModel->SetModel(RES(Model, "Models/0.mdl"));

        digitModel->SetMaterial(RES(Material, "Materials/Bubble.xml"));
        digitModel->SetCastShadows(true);
    }

    timeNode_->Rotate({ -5.f, Vector3::RIGHT });
    timeNode_->SetScale(.42f);
    timeNode_->Translate({ 7.5f * !MC->IsMultiplayer(), 0.f, 4.2f }, TS_WORLD);
}

void GUI3D::CreateSpeedometer()
{
    for (bool first: { true, false })
    {
        Node* speedoNode = node_->CreateChild("Speedometer");
        speedoNode->Translate({ -20/3.f * (1 - (2 * !first)), 0.f, -4.5f }, TS_WORLD);
        speedoNode->SetScale(.8f);
        Speedometer* meter{ speedoNode->CreateComponent<Speedometer>() };
        if (!first)
            meter->SetUrhoTwo();

        if (first)
            speedoNodes_.first_  = speedoNode;
        else
            speedoNodes_.second_ = speedoNode;
    }
}

void GUI3D::Update(float timeStep)
{
    // Bubble wobble
    for (int d{ 10 }; d < 15; ++d)
    {
        const float et{ GetScene()->GetElapsedTime() };
        Node* digit{ clockDigits_[d - 10] };
        if (digit)
        {
            digit->SetScale(.55f + Sin(et * 192.f + d * 23.f) * .034f);
            digit->SetRotation({ 5.f * Cos(et * (101.f - d * 5.f) + d * 128.f), Vector3::RIGHT });
        }
    }

    if (!IsEnabled())
        return;

    const GameState gameState{ MC->GetGameState() };

    // Update clock
    if (gameState == GS_PLAY && time_ > 0u)
    {
        second_ += timeStep;

        if (second_ >= 1.f)
        {
            second_ = 0.f;

            if (toCount_ > 0u)
            {
                Countdown();
            }
            else
            {
                SetTime(time_ - 1u);

                if (time_ == 0u)
                    MC->TimeUp();
            }
        }
    }

    if (gameState == GS_WIN || gameState == GS_LOSE)
    {
        const bool keyConfirm{ INPUT->GetKeyPress(KEY_RETURN)
                            || INPUT->GetKeyPress(KEY_RETURN2)
                            || INPUT->GetKeyPress(KEY_KP_ENTER) };
        bool joyConfirm{ false };
        JoystickState* joy{ INPUT->GetJoystickByIndex(0u) };
        if (joy)
        {
            joyConfirm  = joy->GetButtonPress(CONTROLLER_BUTTON_A);
            joyConfirm |= joy->GetButtonPress(CONTROLLER_BUTTON_START);
        }

        if (keyConfirm || joyConfirm)
        {
            // Next level
            if (gameState == GS_WIN)
                MC->StartNextLevel();
            // Game over
            else if (gameState == GS_LOSE)
                MC->RestartLevel();
        }
    }
}

void GUI3D::Sync()
{
    const CameraProperties camProps{ MC->GetMaze()->GetCameraProperties() };
    const float cameraDistance{ camProps.position_.DistanceToPoint(node_->GetWorldPosition()) };
    node_->SetScale(.105f * cameraDistance);
    node_->SetWorldPosition({ 0.f, 2.f, cameraDistance * .05f - .5f });

    for (Node* scoreNode: { scoreNodes_.first_, scoreNodes_.second_ })
    {
        if (!scoreNode || !scoreNode->IsEnabledSelf())
            continue;

        scoreNode->GetComponent<Scoreboard>()->Sync();

        if (!MC->IsMultiplayer() && scoreNode == scoreNodes_.second_)
            scoreNode->SetEnabledRecursive(false);
    }

    if (time_ != 0u)
    {
        toCount_ = 4u;
        second_ = .17f;
    }
    else
    {
        toCount_ = 0u;
        second_ = 0.f;
    }

    SetTime(time_);
}

void GUI3D::AddPoints(unsigned points, bool first)
{
    if (first)
        scoreNodes_.first_->GetComponent<Scoreboard>()->AddPoints(points);
    else
        scoreNodes_.second_->GetComponent<Scoreboard>()->AddPoints(points);
}

unsigned GUI3D::GetHighestScore() const
{
    Scoreboard* first{ scoreNodes_.first_->GetComponent<Scoreboard>() };
    Scoreboard* second{ (scoreNodes_.second_ ? scoreNodes_.second_->GetComponent<Scoreboard>() : nullptr) };

    if (first && second && second->GetScore() > first->GetScore())
        return second->GetScore();
    else
        return first->GetScore();
}

void GUI3D::OnSetEnabled()
{
    node_->SetEnabledRecursive(IsEnabled());

    if (!IsEnabled())
        return;

    Sync();
}

void GUI3D::Countdown()
{
    toCount_ -= 1u;

    Maze* maze{ MC->GetMaze() };
    const Vector3 central{ node_->WorldToLocal(1/3.f * maze->GetCameraProperties().position_) };
    ValueAnimation* rise{ new ValueAnimation(context_) };
    rise->SetInterpolationMethod(IM_SINUSOIDAL);
    rise->SetKeyFrame(.0f, Vector3::DOWN * maze->GetSize().z_);
    rise->SetKeyFrame(.2f, central);
    rise->SetKeyFrame(.4f, central + Vector3{ 0.f, 1.f, -1.f });

    if (toCount_ > 0u)
    {
        rise->SetKeyFrame(1.f, central + Vector3::UP * maze->GetSize().z_);
    }
    else
    {
        rise->SetKeyFrame(.75f, node_->WorldToLocal(maze->GetCameraProperties().position_) + Vector3::DOWN * .55f);
        rise->SetKeyFrame(1.5f, central * 23.f);
        second_ = -.34f;
    }

    ValueAnimation* grow{ new ValueAnimation(context_) };
    grow->SetInterpolationMethod(IM_SINUSOIDAL);
    grow->SetKeyFrame(0.f, .5f * Vector3::ONE);
    grow->SetKeyFrame(.5f, 1.f * Vector3::ONE);
    grow->SetKeyFrame(1.f + (toCount_ == 0u), 5.f * Vector3::ONE);

    Node* countdownNode{ node_->CreateChild(String(toCount_)) };
    StaticModel* digitModel{ countdownNode->CreateComponent<StaticModel>() };
    digitModel->SetModel(RES(Model, "Models/" + countdownNode->GetName() + ".mdl"));
    digitModel->SetMaterial(RES(Material, "Materials/Bubble.xml"));

    ObjectAnimation* countdownAnim{ new ObjectAnimation(context_) };
    countdownAnim->AddAttributeAnimation("Position", rise, WM_CLAMP, .5f);
    countdownAnim->AddAttributeAnimation("Scale", grow, WM_ONCE, .5f);
    countdownNode->SetObjectAnimation(countdownAnim);

    rise->SetEventFrame(rise->GetKeyFrames().Back().time_, E_RISEN);
    SubscribeToEvent(E_RISEN, DRY_HANDLER(GUI3D, HandleDigitRisen));
}

void GUI3D::HandleDigitRisen(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    Node* node{ static_cast<Node*>(GetEventSender()) };

    if (node)
        node->Remove();
}

void GUI3D::SetTime(unsigned seconds)
{
    second_ = 0.f;
    time_ = seconds;
    unsigned m{ time_ / 60u };
    unsigned s{ time_ % 60u };

    //Update clock graphics
    for (unsigned d{ 0u }; d < 5u; ++d)
    {
        if (d == 2u)
            continue;

        const bool secs{ d < 2u };
        const unsigned p{ PowN(10u, (d - !secs) % 2u) };
        const unsigned cypher{ (secs ? s : m) / p % 10u };

        StaticModel* digitModel{ clockDigits_[d]->GetComponent<StaticModel>() };
        digitModel->SetModel(RES(Model, "Models/" + String(cypher) + ".mdl"));
        clockDigits_[d]->SetEnabled(d < 4u || m >= 10u);
    }
}

bool GUI3D::IsCountingDown() const
{
    return MC->GetGameState() == GS_PLAY && (toCount_ > 0u || second_ < 0.f);
}

void GUI3D::HandleScreenMode(StringHash /*eventType*/, VariantMap& eventData)
{
    const float ratio{ 1.f * eventData[ScreenMode::P_WIDTH].GetInt() / eventData[ScreenMode::P_HEIGHT].GetInt() };
    node_->SetScale(ratio / 1.7f);
}

