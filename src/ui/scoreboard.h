/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SCOREBOARD_H
#define SCOREBOARD_H

#include "../luckey.h"


class Scoreboard: public LogicComponent
{
    DRY_OBJECT(Scoreboard, LogicComponent);

public:
    Scoreboard(Context* context);

    void Update(float timeStep) override;
    void SetUrhoTwo();

    void SetScore(unsigned score);
    unsigned GetScore() const { return score_; }
    void AddPoints(unsigned points);
    void Penalty(unsigned points);

    void StartLevel()
    {
        startScore_ = score_;
    }

    void RestartLevel()
    {
        score_ = startScore_;
    }

    void Sync() { SetScore(score_); }

protected:
    void OnNodeSet(Node* node) override;

private:
    void SubscribeToEvents();
    void HandleUrhoHit(StringHash eventType, VariantMap& eventData);

    HashMap<int, Node*> scoreDigits_;
    unsigned score_;
    unsigned startScore_;
    bool urhoOne_;

};

#endif // SCOREBOARD_H
