/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../environment/maze.h"

#include "urchin.h"

Urchin::Urchin(Context* context): Hazard(context),
    modelNode_{},
    target_{},
    clockwise_{}
{
}

void Urchin::OnNodeSet(Node* node)
{
    if (!node)
        return;

    modelNode_ = node_->CreateChild("Model");
    AnimatedModel* urchinModel{ modelNode_->CreateComponent<AnimatedModel>() };
    urchinModel->SetModel(CACHE->GetResource<Model>("Models/Urchin.mdl"));
    urchinModel->SetCastShadows(true);
    urchinModel->SetMaterial(CACHE->GetResource<Material>("Materials/VColHighSpec.xml"));
}

void Urchin::Set(const Vector3& position, const Quaternion& rotation)
{
    SceneObject::Set(position, rotation);
    target_ = node_->GetWorldPosition();
}

void Urchin::Update(float timeStep)
{
    Hazard::Update(timeStep);

    const Vector3 oldPos{ node_->GetPosition() };
    node_->SetWorldPosition(node_->GetPosition().Lerp(target_, timeStep * Sqrt(target_.DistanceToPoint(node_->GetWorldPosition())) * 4.2f));
    modelNode_->Rotate({ (360.f * (node_->GetPosition() - oldPos) / M_PI).Length(), node_->GetWorldRight() }, TS_WORLD);

    if (target_.DistanceToPoint(node_->GetWorldPosition()) < .055f)
    {
        const Quaternion turn{ Quaternion(90.0f, Vector3::UP) };
        modelNode_->Rotate(turn.Inverse(), TS_WORLD);
        node_->Rotate(turn, TS_WORLD);

        target_ += node_->GetDirection();
    }
}



