/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef JELLY_H
#define JELLY_H

#include "hazard.h"

class Jelly: public Hazard
{
    DRY_OBJECT(Jelly, Hazard);

public:
    Jelly(Context* context);
    void OnNodeSet(Node* node) override;
    void Update(float timeStep) override;

    void Set(const Vector3& position, const Quaternion& rotation) override;

private:
    Node* modelNode_;
    Vector3 target_;

    AnimationController* animCtrl_;
};

#endif // JELLY_H
