/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../environment/maze.h"
#include "../urho.h"

#include "anglerfish.h"

Anglerfish::Anglerfish(Context* context): Hazard(context),
    modelNode_{ nullptr },
    animCtrl_{ nullptr },
    territory_{},
    roamTarget_{}
{
    radius_ = 1.1f;
}

void Anglerfish::OnNodeSet(Node* node)
{
    if (!node)
        return;

    animCtrl_ = node_->CreateComponent<AnimationController>();

    modelNode_ = node_->CreateChild("Model");
    AnimatedModel* anglerModel{ modelNode_->CreateComponent<AnimatedModel>() };
    anglerModel->SetCastShadows(true);
    anglerModel->SetModel(RES(Model, "Models/Anglerfish.mdl"));
    anglerModel->SetMaterial(RES(Material, "Materials/Anglerfish.xml"));

    Node* lampNode{ modelNode_->CreateChild("Lamp") };
    lampNode->SetPosition({ 0.f, .75f, 1.25f });
    Light* lamp{ lampNode->CreateComponent<Light>() };
    lamp->SetBrightness(.55f);
    lamp->SetColor(Color::CYAN.Lerp(Color::WHITE, .17f));
    lamp->SetRange(2.3f);
    lamp->SetCastShadows(true);
    lamp->SetShadowResolution(1/4.f);
}

void Anglerfish::Set(const Vector3& position, const Quaternion& rotation)
{
    Hazard::Set(position, rotation);

    roamTarget_ = node_->GetWorldPosition();
}

void Anglerfish::Update(float timeStep)
{
    Hazard::Update(timeStep);

//    bool seenUrho{ false };
    const Vector3 toTarget{ (/*seenUrho ? Urho::urhos_->GetWorldPosition()
                                      :*/ roamTarget_) - node_->GetWorldPosition() };
    Quaternion toTargetRotation{};
    toTargetRotation.FromLookRotation(toTarget.NormalizedOrDefault(node_->GetWorldDirection()));
    node_->SetWorldRotation(node_->GetWorldRotation().Slerp(toTargetRotation, timeStep * 2.f));
    node_->Translate(.23f * node_->GetWorldDirection().DotProduct(toTarget.Normalized()) * node_->GetWorldDirection() * timeStep * 3.f, TS_WORLD);

    if (node_->GetWorldPosition().DistanceToPoint(roamTarget_) < .1f)
    {
        Maze* maze{ MC->GetMaze() };

        HashSet<IntVector2>::Iterator it{ territory_.Begin() };
        for (int t{ Random(static_cast<int>(territory_.Size())) }; t > 0; --t)
            ++it;

        roamTarget_ = maze->CoordsToPosition({ it->x_, 1, it->y_ }) + Vector3{ .5f, 0.f, .5f };
    }
}

void Anglerfish::MapTerritory()
{
    territory_.Clear();
    Maze* maze{ MC->GetMaze() };

    const IntVector2 size{ 2, 2 };
    const IntVector3 nearestCoords{ maze->WorldPositionToCoords(node_->GetWorldPosition()) };
    const IntVector2 nearestSquare{ nearestCoords.x_, nearestCoords.z_ };
    HashSet<IntVector2> freeSquares{ maze->GetFreeSquares() };
    if (!freeSquares.Contains(nearestSquare))
        return;

    HashSet<IntVector2> fitSquares{ freeSquares };

    for (const IntVector2& coords: freeSquares)
    {
        for (const IntVector2 c: { coords,
                                   coords + IntVector2{ 1, 0 },
                                   coords + IntVector2{ 0, 1 },
                                   coords + IntVector2{ 1, 1 } })
        {
            if (!freeSquares.Contains(c))
                fitSquares.Erase(coords);
        }
    }

    PODVector<IntVector2> connected{ nearestSquare };
    for (unsigned i{ 0u }; i < connected.Size(); ++i)
    {
        for (const IntVector2 c: { connected.At(i) + IntVector2{  1,  0 },
                                   connected.At(i) + IntVector2{ -1,  0 },
                                   connected.At(i) + IntVector2{  0,  1 },
                                   connected.At(i) + IntVector2{  0, -1 } })
        {
            if (fitSquares.Contains(c) && !connected.Contains(c))
                connected.Push(c);
        }
    }

    for (const IntVector2& coords: fitSquares)
    {
        if (!connected.Contains(coords))
            fitSquares.Erase(coords);
    }

    territory_ = fitSquares;
    if (territory_.IsEmpty())
        Disable();
}

bool Anglerfish::hitsUrho(Urho* urho)
{
    const Vector3 toUrho{ urho->GetWorldPosition() - node_->GetWorldPosition() };
    return toUrho.Length() < radius_ * Lerp(1.f, toUrho.Normalized().DotProduct(node_->GetWorldDirection()), .23f);
}
