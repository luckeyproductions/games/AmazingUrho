/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../environment/maze.h"

#include "barrel.h"

StaticModelGroup* Barrel::barrelGroup_{};

Barrel::Barrel(Context* context): Hazard(context)
{
}

void Barrel::OnNodeSet(Node* node)
{
    if (!node)
        return;

    if (!barrelGroup_)
    {
        barrelGroup_ = node_->GetScene()->CreateComponent<StaticModelGroup>();
        barrelGroup_->SetModel(CACHE->GetResource<Model>("Models/Barrel.mdl"));
        barrelGroup_->SetMaterial(CACHE->GetResource<Material>("Materials/VColHighSpec.xml"));
        barrelGroup_->SetCastShadows(true);
    }

    modelNode_ = node_->CreateChild("Model");
    modelNode_->Rotate(Quaternion(Random(360.f), Random(2) ? 180.f : 0.f, 0.f));
}

void Barrel::Set(const Vector3 &position, const Quaternion& rotation)
{
    SceneObject::Set(position, rotation);

    barrelGroup_->AddInstanceNode(modelNode_);
}

void Barrel::Disable()
{
    SceneObject::Disable();

    barrelGroup_->RemoveInstanceNode(modelNode_);
}

void Barrel::Update(float timeStep)
{
    Hazard::Update(timeStep);

    const Vector3 worldPos{ node_->GetWorldPosition() };
    const float pitDistance{ PitDistance() };

    if (worldPos.y_ > 4/3.f)
    {
        node_->Translate(Vector3::DOWN * Min(timeStep * 4.2f, worldPos.y_ - 4/3.f));
        modelNode_->Rotate(Quaternion(timeStep * 100.f, Vector3::RIGHT), TS_PARENT);
    }
    else
    {
        node_->Translate(Vector3::FORWARD * timeStep * Min(pitDistance * 5.f, 1.666f));
        modelNode_->Rotate(Quaternion(timeStep * 235.f, Vector3::RIGHT), TS_PARENT);
    }

    if (pitDistance < .23f)
        node_->Translate(Vector3::DOWN * timeStep * (17.f - 77.f * pitDistance));

    if (node_->GetWorldPosition().y_ < -5.f)
        Disable();
}

float Barrel::PitDistance()
{
    for (const Vector3& pitPos: MC->GetMaze()->GetPits())
    {
        float distance{ (LucKey::ProjectOntoPlane(pitPos - node_->GetWorldPosition(),
                                                  Vector3::ZERO, Vector3::UP)).LengthSquared()};
        if (distance < 1.f)
            return Sqrt(distance);
    }

    return M_INFINITY;
}



