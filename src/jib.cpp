/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "jib.h"

Jib::Jib(Context* context): LogicComponent(context),
    camera_{ nullptr },
    renderPath_{ nullptr }
{
}

void Jib::OnNodeSet(Node* node)
{
    if (!node)
        return;

    AUDIO->SetListener(node_->CreateComponent<SoundListener>());
    camera_ = node_->CreateComponent<Camera>();

    Viewport* viewport{ new Viewport(context_, GetScene(), camera_) };
    SharedPtr<RenderPath> blurred{ viewport->GetRenderPath()->Clone() };
    renderPath_ = blurred;
    blurred->Append(RES(XMLFile, "PostProcess/Blur.xml"));
    blurred->SetEnabled("Blur", false);
    viewport->SetRenderPath(blurred);

    RENDERER->SetViewport(0, viewport);
}

void Jib::Set(CameraProperties camProps, bool resetBlur)
{
    node_->SetPosition(camProps.position_);
    node_->LookAt(camProps.lookTarget_);
    camera_->SetFov(camProps.fieldOfView_);

    if (resetBlur)
        DisableBlur();
}

void Jib::DisableBlur()
{
    renderPath_->SetEnabled("Blur", false);
}

void Jib::Update(float timeStep)
{
    const bool isBlurred{ renderPath_->IsEnabled("Blur") };
    const bool shouldBlur{ MC->GetGameState() == GS_WIN || MC->GetGameState() == GS_LOSE };

    if (!isBlurred && shouldBlur)
    {
        renderPath_->SetEnabled("Blur", true);
        sinceBlur_ = 0.f;
    }

    if (!renderPath_->IsEnabled("Blur"))
        return;

    renderPath_->SetShaderParameter("BlurRadius", Sqrt(Min(4.f, sinceBlur_ * 5.f)));
    sinceBlur_ += timeStep;
}



