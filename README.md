# A Mazing Urho

The **[LucKey Productions](https://luckey.games)** entry for the Open Jam 2018 featuring [Urho](https://urho3d.io/about.html#origin-of-the-name) swimming through a maze.

The theme of the jam was: **Spam to win!**

## Screenshots
![Screenshot](Screenshots/Screenshot_Tue_Oct__9_01_51_54_2018.png)

![Screenshot](Screenshots/Screenshot_Sun_Oct__7_20_30_09_2018.png)

## Used software

- [Linux Mint](https://linuxmint.com/)
- [Dry](https://dry.luckey.games/)
- [Blender](https://www.blender.org/)
- [GIMP](https://www.gimp.org/)
- [JACK](http://www.jackaudio.org/)
- [VCV Rack](https://vcvrack.com/)
- [Calf Fluidsynth](https://calf-studio-gear.org/)
- [Calf Reverb](https://calf-studio-gear.org/)
- [QTractor](http://qtractor.org/)
- [SuperCollider](https://supercollider.github.io/)
- [Audacity](https://www.audacityteam.org/)
- [Gitter](https://gitter.im)
- [QtCreator](https://www.qt.io/)

## Credits

- **[Modanung](https://libregamewiki.org/Modanung)** - Graphics, code and samples
- **[kostik1337](https://kostik1337.itch.io/)** - Music and samples
