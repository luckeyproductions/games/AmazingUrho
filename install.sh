#!/bin/sh

./.installreq.sh
./.builddry.sh

git pull
qmake AmazingUrho.pro -o ../AmazingUrho-build/; cd ../AmazingUrho-build
sudo make install; cd -; ./.postinstall.sh
rm -rf ../AmazingUrho-build
